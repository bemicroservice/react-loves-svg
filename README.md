# React loves SVG
This sample demonstrates SVG visualization for IBM stock price changes - single market day in 5 minutes intervals. The graph is pure SVG coded in React and Sass - no extra libraries.

## What is SVG
Scalable vector graphics (SVG) is alternative to common bitmap images (JPG, PNG). Instead of keeping color information for every pixel, SVG is represended by instructions "how to draw the image". The browser reads this "instruction manual" and render the image. Picture dimensions are irelevant, SVG use the same source for every device with minor performance effect.

### SVG can be coded
In fact SVG is not a image format, but markup language with standard declaration based on XML tags - like HTML. It means, you can write your images just by code in any editor. 

SVG code is composition of simple shapes (line, text, path, circle, retangle etc.) and CSS styling. Guess what - SASS or another CSS preprocessors support SVG styling properties, so you can use advanced styling expressions like mixins with media queries.

## SVG with React?
Although React is designed for HTML DOM, the SVG format is supported in JSX / TSX code including class names or styling.

Your SVG render can work with component states, use fetched data from API, conditional rendering or interact with event handlers by user interactions.

No extra libraries or code techniques are required. Sure, when you want to spare development time, you can use some for specific renders like plots / charts, but it is optional.


## Used technologies
- React.js (SPA, Typescript)
- SVG (responsive)
- Sass for styling

## Install
1. Install Node.js with NPM
2. Clone the repository
3. Run `npm i` for package installation
4. Run `npm start` for app start