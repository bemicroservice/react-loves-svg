import { stockDataFetch } from "../services/StockDataApi";

describe("Check data fixture", () => {

  it("Loads IBM fixture", () => {
    const result = stockDataFetch()
    expect(result).toHaveProperty("ticker", "IBM")
    expect(result.series.length).toEqual(100)
    expect(result.timePoints.length).toEqual(100)
    expect(result.minMax[0]).toEqual(131.4)
    expect(result.minMax[1]).toEqual(137.16)
  })
  
})
