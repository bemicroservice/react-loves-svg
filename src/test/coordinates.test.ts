import { spaceCoordinateToStockValue, stockDataToCoordinates, xValueConvert, yValueConvert } from "../utils/CoordinatesUtils"

describe("Check coordinates", () => {

  it("Check stock value converting", () => {
    const checkValues = [30, 31, 32, 33, 34]
    const expectedResults = [100, 75, 50, 25, 0] 
    const minMax: [number, number] = [30, 34]
    for (let index = 0; index < checkValues.length; index++) {
      const result = yValueConvert(checkValues[index], minMax)
      expect(result).toEqual(expectedResults[index])
    }
  })

  it("Check time value converting", () => {
    const timepoints = ["2022-01-25 20:00:00", "2022-01-25 19:55:00", "2022-01-25 19:40:00", "2022-01-25 19:35:00", "2022-01-25 19:35:00"]
    const expectedResults = [0, 25, 50, 75, 100]
    for (let index = 0; index < timepoints.length; index++) {
      const result = xValueConvert(index, timepoints)
      expect(result).toEqual(expectedResults[index])
    }
  })

  it("Check whole stock records converting", () => {
    const timepoints = ["2022-01-25 20:00:00", "2022-01-25 19:55:00", "2022-01-25 19:40:00", "2022-01-25 19:35:00", "2022-01-25 19:35:00"]
    const checkValues = [30, 31, 32, 33, 34]
    const minMax: [number, number] = [30, 34]
    const expectedResults: Array<[number, number]> = [[0,100], [25,75], [50, 50], [75, 25], [100, 0]]
      
    const results = stockDataToCoordinates(minMax, checkValues, timepoints)
    for (let index = 0; index < results.length; index++) {
      const [x,y] = results[index]
      const expected = expectedResults[index]
      expect(x).toEqual(expected[0])      
      expect(y).toEqual(expected[1])      
    }
  })

  it("Check convert GUI coordinate to stock price", () => {
    const checkValues = [10, 20, 50]
    const expectedValues = [20, 30, 60]
    const minMax: [number, number] = [10, 110]
      
    for (let index = 0; index < checkValues.length; index++) {
      const result = spaceCoordinateToStockValue(checkValues[index], minMax)
      expect(result).toEqual(expectedValues[index])      
    }
  })
})