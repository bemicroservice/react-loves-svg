import { StockDataRecords } from "../../services/StockDataApi";


export default (props: StockDataRecords) => {

  const renderTableRows = () => props.priceMoves.map((stockPriceMove, index) => {
    const evenStyle = index % 2 === 0 ? "even" : "odd"
    const { time, move, movePercent, value } = stockPriceMove
    return (
      <tr className={evenStyle}><td>{time}</td><td>{value}</td><td>{move}</td><td>{movePercent}</td></tr>
    )
  })

  return(
    <table>
      <thead>
        <tr> <th>Time</th> <th>Value</th>  <th>Move</th> <th>Move (%)</th> </tr>
      </thead>
      <tbody>
        {renderTableRows()}
      </tbody>
    </table>
  )

}