import { StockDataRecords } from "../../services/StockDataApi"
import BaseSvg from "./BaseSvg"
import Axis from "./Axis"
import StockLine from "./StockLine"

interface LineStockChartProps {
  stockData: StockDataRecords
}

/**
 * Line chart for the stock trading day
 */
export default (props: LineStockChartProps) => {

  const {stockData} = props

  return (
    <BaseSvg id="stock-chart">
      <Axis type="x" data={stockData} />
      <Axis type="y" data={stockData} />
      <StockLine stockData={stockData} />
    </BaseSvg>
  )
}