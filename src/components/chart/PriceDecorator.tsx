import { useState } from "react"
import { unitTransformation } from "../../test2"
import { toPercent, xValueConvert, yValueConvert } from "../../utils/CoordinatesUtils"

interface PriceDecoratorProps {
  coordinates: [number, number]
  priceValue: number
  timeValue: string
  minMax: [number, number]
}

interface PriceDecoratorState {
  active: boolean,
}

/**
 * Price decorator to highlight current stock price on chart
 */
export default (props: PriceDecoratorProps) => {
  const [decoratorState, setDecoratorState] = useState<PriceDecoratorState>({ active: false })
  const { coordinates, minMax, priceValue } = props
  const [min, max] = minMax
  const [x, y] = unitTransformation([...coordinates], toPercent)
  
  const selectHighlight = () => {
    switch(priceValue) {
      case min:
        return "min"
      
      case max:
      return "max"

      default:
        return "normal";
    }
  } 

  return (
    <g>
      {renderDecoratorText(decoratorState.active, props.timeValue, props.priceValue)}
      {renderDecoratorLine(decoratorState.active, y, x)}
      <circle
        cx={x}
        cy={y}
        r={decoratorState.active && ".5rem" || ".3rem"}
        className={`price-decorator price-decorator-${selectHighlight()}`}
        onMouseEnter={() => setDecoratorState({ ...decoratorState, active: true })}
        onMouseLeave={() => setDecoratorState({ ...decoratorState, active: false })}
      />
    </g>
  )
}

/**
 * Render text information about selected price record
 * @param isVisible 
 * @param props 
 * @returns 
 */
const renderDecoratorText = (isVisible: boolean, timeValue: string, priceValue: number) => {
  const textPointCoordinates = ["90%", "4%", "9%"]

  if (isVisible)
    return (
      <g>
        <text
          x={textPointCoordinates[0]}
          y={textPointCoordinates[1]}
          className="price-decorator-text">
          {`Price: ${priceValue}`}
        </text>
        <text
          x={textPointCoordinates[0]}
          y={textPointCoordinates[2]}
          className="price-decorator-text">
          {`Time: ${timeValue}`}
        </text>
      </g>

    )
}

/**
 * Render y-axis guidline for selected stock price record
 * @param isVisible 
 * @param yValuePercent Value in percents for set guildline coordinations
 * @returns 
 */
const renderDecoratorLine = (isVisible: boolean, yValuePercent: string, xValuePercent: string) => {

  if (isVisible)
    return (
      <g>
        <line
          x1="0%"
          x2="100%"
          y1={yValuePercent}
          y2={yValuePercent}
          className="price-decorator-line"
        />
        <line
          y1={yValuePercent}
          y2="100%"
          x1={xValuePercent}
          x2={xValuePercent}
          className="price-decorator-line"
        />
      </g>

    )
}


