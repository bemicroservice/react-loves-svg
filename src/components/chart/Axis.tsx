import { StockDataRecords } from "../../services/StockDataApi";
import { convertUnits, toPercent, spaceCoordinateToStockValue } from "../../utils/CoordinatesUtils";


interface AxisProps {
  data: StockDataRecords
  type: "x" | "y"
}

/**
 * Component for axis lines (X or Y)
 */
export default (props: AxisProps) => {
  
  const X_AXIS_POINTS: [[number, number], [number, number]] = [[0, 100], [100, 100]]
  const Y_AXIS_POINTS: [[number, number], [number, number]] = [[0, 0], [0, 100]]
  
  const [point1, point2] = props.type === "x" ? X_AXIS_POINTS : Y_AXIS_POINTS
  const [x1, y1, x2, y2] = convertUnits([...point1, ...point2], toPercent)

  return (
    <g>
      <line
        x1={x1}
        x2={x2}
        y1={y1}
        y2={y2}
        id={`axis-${props.type}`}
        className="axis-line" />
      {renderAllMarkers(props)}
    </g >
  )

}


/**
 * Render all marker on the axis
 * @param props Props for the axis
 * @returns Collection of markers (line and text)
 */
const renderAllMarkers = (props: AxisProps) => {
  const { series, timePoints, minMax } = props.data

  const renderLines = []

  const extractTimepointValue = (index: number) => timePoints[index].substring(0, 5)
  const extractStockByIndexValue = (index: number) => spaceCoordinateToStockValue(index, minMax).toString()

  for (let index = 0; index < series.length; index += 20) {
    const value = props.type === "x" ? extractTimepointValue(index) : extractStockByIndexValue(index)
    renderLines.push(drawMarker(props.type, index, value && value.toString()))
  }

  return renderLines
}

/**
 * Draw one marker - line and text of the value
 * @param type Axis type (X or Y)
 * @param moveFromZeroPercent Step from beginning
 * @param value Value for the text
 * @returns One single marker - grouped line and text 
 */
const drawMarker = (type: "x" | "y", moveFromZeroPercent: number, value: string) => {

  const xPoints: [[number, number], [number, number]] = [[0 + moveFromZeroPercent, 95], [0 + moveFromZeroPercent, 100]]
  const yPoints: [[number, number], [number, number]] = [[0, 100 - moveFromZeroPercent], [.5, 100 - moveFromZeroPercent]]

  const [point1, point2] = type === "y" ? yPoints : xPoints
  const [x1, y1, x2, y2] = convertUnits([...point1, ...point2], toPercent)

  return (
    <g>
      <line
        y1={y1}
        y2={y2}
        x1={x1}
        x2={x2}
        className={`axis-limit-line axis-limit-line-${type}`}
      />
      {moveFromZeroPercent > 0 && <text x={x1} y={y1} className={`axis-text axis-text-${type}`}>{value}</text>}
    </g>

  )
}