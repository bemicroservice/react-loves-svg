import { StockDataRecords } from "../../services/StockDataApi"
import { convertUnits, stockDataToCoordinates, toPercent } from "../../utils/CoordinatesUtils"
import PriceDecorator from "./PriceDecorator"

interface StockLineProps {
  stockData: StockDataRecords
}

/**
 * Collections of lines (moves) on the market during time period and dynamic price decorators
 */
export default (props: StockLineProps) => {

  const { minMax, series, timePoints } = props.stockData

  /**
   * Render line for single price move
   * @param priceLineCoordinates array of coordinates in order (x1, y1, x2, y2)
   * @returns Line rendered element for price move
   */
  const renderSinglePriceLine = (priceLineCoordinates: number[]) => {
    const [x1, y1, x2, y2] = convertUnits(priceLineCoordinates, toPercent)

    return (
      <line
        x1={x1}
        y1={y1}
        x2={x2}
        y2={y2}
        className="stock-line"
      />)
  }

  /**
   * Render single price decorator by coordinates and index
   * @param coordinates Coordinates, where is the decorator
   * @param index Index of the price value
   * @returns 
   */
  const renderSinglePriceDecorator = (coordinates: [number, number], index: number) =>
    <PriceDecorator
      coordinates={coordinates}
      priceValue={series[index]}
      timeValue={timePoints[index]}
      minMax={minMax}
    />


  /**
    * Generate all price moves
    * @returns Collection of SVG lines
    */
  const renderPriceMoves = () => {

    const pointCoordinates = stockDataToCoordinates(minMax, series, timePoints)
    const priceLines = []
    const priceDecorators = []

    for (let index = 0; index < pointCoordinates.length; index++) {
      const [x, y] = pointCoordinates[index];

      const lineCoordinates =
        index === 0 ? [x, y, ...pointCoordinates[index + 1]] : [...pointCoordinates[index - 1], x, y]

      priceLines.push(renderSinglePriceLine(lineCoordinates))
      priceDecorators.push(renderSinglePriceDecorator([x, y], index))
    }

    return { priceLines, priceDecorators }
  }


  const { priceLines, priceDecorators } = renderPriceMoves()
  return (
    <g>
      {priceLines}
      {priceDecorators}
    </g>
  )
}




