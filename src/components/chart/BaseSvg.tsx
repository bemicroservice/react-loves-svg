interface SvgProps{
  viewboxZoom?: number,
  children?: any,
  id: string
}

/**
 * Base SVG responsive container
 */
export default (props: SvgProps) => 

    <svg id={props.id} xmlns="http://www.w3.org/2000/svg">
      {props.children}
    </svg>
