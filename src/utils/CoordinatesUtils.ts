import { roundValue } from "./MathUtils"

/**
 * Converts numeric value to percentage format value
 * @param nummericValue Original numeric value
 * @returns Percentage fromat value
 */
export const toPercent = (nummericValue: number): string => `${nummericValue}%`

/**
 * Transform array of numberic values by choosen function
 * @param values Original numberic values
 * @param transformFunc Transformation function
 * @returns Array of formated values
 */
export const convertUnits = (values: number[], transformFunc: (original: number) => string) => {
  return values.map(val => transformFunc(val))
}

/**
 * Transform stock numeric data (value and time) to SVG coordinate values (points in linear space) 
 * @param minMax Minimum and maximum of stock values
 * @param series Array of all stock values 
 * @param timePoints Array of time intervals for the values
 * @returns Collection of coordinates [x,y] for SVG
 */
export const stockDataToCoordinates = (minMax: [number, number], series: number[], timePoints: string[]) => {
  const results: Array<[number, number]> = []
  for (let index = 0; index < series.length; index++) {
    const stockValue = series[index]

    const x = xValueConvert(index, timePoints)
    const y = yValueConvert(stockValue, minMax)

    results.push([x, y])
  }
  return results
}

/**
 * Convert stock value to Y axis coordinate
 * @param value Stock value
 * @param valueMinMax Minimum and maximum of all stock values
 * @returns Y axis value fot the stock price
 */
export const yValueConvert = (value: number, valueMinMax: [number, number]): number => {
  const [min, max] = valueMinMax

  if (value > max || value < min)
    throw new Error(`Value ${value} is out of range`);

  const fullDelta = max - min
  const localDelta = max - value

  //SVG has start of coordinates in top-left corner. Y axis raises from top to bottom
  const yCor = (localDelta / fullDelta) * 100
  return yCor
}

/**
 * Convert time interval value to X axis SVG coordinate
 * @param index Ordinal number in line of time intervals
 * @param timepoints Array of all time intervals
 * @returns Coordinate do the time on X axis
 */
export const xValueConvert = (index: number, timepoints: Array<string>): number => {

  const max = timepoints.length - 1
  const localDelta = max - index
  const xCor = 100 - (localDelta / max) * 100

  return xCor
}

/**
 * Convert percent part of the coordinate Y space to stock value
 * @param spaceFromTopPercent How much percent from the top the values is
 * @param minMax Minimum and maximum of all stock values
 * @returns Stock value
 */
export const spaceCoordinateToStockValue = (spaceFromTopPercent: number, minMax: [number, number]) => {
  const[min, max] = minMax
  const relativeMax = max - min
  const onePercentOfTrack = (relativeMax / 100)
  return roundValue((spaceFromTopPercent * onePercentOfTrack) + min, 2)
}