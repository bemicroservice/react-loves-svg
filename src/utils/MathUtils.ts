/**
 * Round float number to selected number of digits
 * @param rawNumber Raw float number before round
 * @param numberOfDigits Number of decimal digits after round operation
 * @returns Rounded float number
 */
export const roundValue = (rawNumber: number, numberOfDigits = 2) => parseFloat(rawNumber.toFixed(numberOfDigits))