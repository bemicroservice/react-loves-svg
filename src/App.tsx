import { useEffect, useState } from 'react';
import './styles/App.sass';
import './styles/Chart.sass';

import AllValuesTable from './components/data-overview/AllValuesTable';
import StockChart from './components/chart/StockChart';
import { stockDataFetch, StockDataRecords } from './services/StockDataApi';

interface AppState {
  stockRecords: StockDataRecords
}

function App() {

  const [state, setState] = useState<AppState>()

  useEffect(() => {
    const data = stockDataFetch()
    setState(() => ({ stockRecords: data }))
  }, [])

  return (
    <main className="App">
      <section className='summary'>
        <aside className='chart'>
          {
            state && <StockChart stockData={state.stockRecords} />
          }
        </aside>
      
      </section>

      <section className='all-values'>
        {
          state && <AllValuesTable {...state?.stockRecords} />
        }
      </section>
    </main>
  );
}

export default App;
