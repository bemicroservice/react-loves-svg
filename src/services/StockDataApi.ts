import { roundValue } from "../utils/MathUtils"

export interface StockDataRecords {
  ticker: string
  date: string
  intervalMinutes: number
  timePoints: string[]
  minMax: [number, number]
  series: number[]
  priceMoves: StockPriceMove[]
}

export interface StockPriceMove {
  time: string
  value: number
  move: string
  movePercent: string
}

/**
 * Fetch stock data from source and convert into supported data model
 * @returns Collection of IBM stock data
 */
export const stockDataFetch = (): StockDataRecords => {

  // fixture data source
  const rawData: any = require(`../assets/fixtures/ibm.json`)
  const metadataRaw = rawData["Meta Data"]

  const ticker = metadataRaw["2. Symbol"] as string

  const dateRaw = metadataRaw["3. Last Refreshed"] as string
  const date = dateRaw.split(" ")[0]

  const intervalMinRaw = metadataRaw["4. Interval"] as string

  if (!intervalMinRaw.includes("min"))
    throw new Error("Wrong interval format");

  const priceInterval = parseInt(intervalMinRaw.replace("min", ""))

  const timeseriesRaw = rawData["Time Series (5min)"]
  const [series, timePoints, priceMoves] = processStockRecords(timeseriesRaw)

  return {
    ticker,
    date,
    series,
    timePoints: timePoints,
    priceMoves: priceMoves,
    intervalMinutes: priceInterval,
    minMax: [Math.min(...series), Math.max(...series)],
  }
}

/**
 * Process stock price records to series, timepoints and price moves
 * @param timeseriesRaw Raw stock data
 * @returns Tuple of processed data 
 */
const processStockRecords = (timeseriesRaw: any): [number[], string[], StockPriceMove[]] => {

  const series: number[] = []
  const timePoints: string[] = []
  const priceMoves: StockPriceMove[] = []

  let index = 0
  const orderedKeys = Object.keys(timeseriesRaw).reverse()
  for (const rawDate of orderedKeys) {
    const time = rawDate.split(" ")[1] as string
    let stockValueRaw = parseFloat(timeseriesRaw[rawDate]["1. open"])
    const stockValue = roundValue(stockValueRaw)

    timePoints.push(time)
    series.push(stockValue)

    if (index === 0)
      priceMoves.push({ move: "+0", movePercent:"0%", time, value: stockValue })
    else {
      const { value: previousValue } = priceMoves[index - 1]
      const difference = roundValue(stockValue - previousValue)
      const differencePercent = roundValue((difference / previousValue) * 100) + "%"
      const record = difference < 0 ? `${difference}` : `+${difference}`
      priceMoves.push({ move: record, movePercent: differencePercent, time, value: stockValue })
    }

    index++
  }

  if (series.length !== timePoints.length || priceMoves.length !== series.length)
    throw new Error("Data mapping error");

  return [series, timePoints, priceMoves]
}